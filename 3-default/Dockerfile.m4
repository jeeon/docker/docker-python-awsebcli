#
# Use standard Ruby base image
#

FROM python:3-stretch

include(common/maintainer.dockerfile)

include(common/install-awsebcli.dockerfile)
