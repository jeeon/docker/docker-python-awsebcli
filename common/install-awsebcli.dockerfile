#
# Install AWS Elastic Beanstalk CLI
#

ENV AWSEBCLI_VERSION=3.14.13

RUN pip install awsebcli==$AWSEBCLI_VERSION
